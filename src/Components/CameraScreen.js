import React, { Component } from 'react';
import { StyleSheet, View, AlertIOS } from 'react-native';
import { RNCamera } from 'react-native-camera';

import PendingView from './PendingView'
import ButtonBlock from './ButtonBlock'

export default class CameraScreen extends Component {
  state = {
      isRecord: false
  }

  handleTakeVideo = async (camera) => {
    this.setState({isRecord: true})
    const data = await camera.recordAsync();
    AlertIOS.alert('File path', JSON.stringify(data.uri))
  };

  handleStopVideo = async (camera) => {
    this.setState({isRecord: false})
    camera.stopRecording();
  }

  render() {
    const { isRecord } = this.state
    return (
      <View style={styles.container}>
        <RNCamera
          style={styles.preview}
          type={RNCamera.Constants.Type.back}
          flashMode={RNCamera.Constants.FlashMode.on}
          defaultVideoQuality={ RNCamera.Constants.VideoQuality["1080p"] }
          codec={RNCamera.Constants.VideoCodec['H264']}
          permissionDialogTitle='Permission to use camera'
          permissionDialogMessage='We need your permission to use your camera phone'
        >
          {({ camera, status }) => {
            if (status !== 'READY') return <PendingView />;
            return (
              <ButtonBlock 
                isRecord={isRecord}
                onStartVideo={this.handleTakeVideo.bind(this, camera)}
                onStopVideo={this.handleStopVideo.bind(this, camera)}
              />
            )
          }}
        </RNCamera>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'black',
    position: 'relative'
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  }
});
